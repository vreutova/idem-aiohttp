import aiohttp.web
import pytest


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)

    yield hub


@pytest.fixture(scope="function", autouse=True)
async def cleanup_hub(hub):
    hub.tool.request.application.APP.freeze()
    await hub.tool.request.application.APP.shutdown()
    hub.tool.request.application.APP = aiohttp.web.Application()


@pytest.fixture(scope="session")
def acct_subs():
    return ["request"]


@pytest.fixture(scope="session")
def acct_profile():
    return "test_idem_aiohttp"
